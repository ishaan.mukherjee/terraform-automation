from resource_store import resource_store, resource_store_map
import subprocess
import json

ADD = 'ADD'
DEL = 'DEL'
UPDATE = 'UPDATE'

def print_dictionary():
    reference = ''
    for key in resource_store:
        new_reference = resource_store_map[resource_store[key].__name__]
        if new_reference == reference:
            print(reference, " -> ", key)
        else:
            print("\n")
            print(new_reference, " -> ", key)
            reference = new_reference

def print_help_screen():

    print("\n# Create terraform files from your CLI. To get started:")
    print("# Initialize a .tf file")
    print("$ init <terraform_file_name>")
    print("# Create a resource group with the local name 'main'")
    print("$ rgp main")
    print("\n# Variables can be referenced using a shorter format.")
    print("# Eg: To reference the resource group's name: rgp.main.name")
    print("# Type 'dict' for a dictionary of resources")



def check_resource_type(request_string: str) -> None:
    """
    Checks the resource name and calls the resource creation method.
    """
    request = request_string.split(" ")
    try:
        response = resource_store[request[0]](request[1])
        print("\n" * 10)
        response.print_response()
        x = subprocess.run("\"C:\\Users\\imukherjee032\\Downloads\\terraform.exe\" fmt", capture_output=True)
    except:
        if request[0] == "exit":
            return
        elif request[0] == "help":
            print_help_screen()
            return
        elif request[0] == "dict":
            print_dictionary()
            return
        else:
            print("Unfamiliar command. Please try again")
            return