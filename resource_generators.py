from classes import Response
from resource_template import *




def create_resource_group(local_name: str) -> Response:
    """
    Creates a resource group.
    """
    return resource_template(local_name, ["azurerm_resource_group",
        [
            ("name", 'STR'),
            ("location", 'STR')
        ]
    ])

def create_virtual_network(local_name: str) -> Response:
    """
    Creates a virtual network resource.
    """
    return resource_template(local_name, ["azurerm_virtual_network",
        [
            ("name", 'STR'),
            ("location", 'STR'),
            ("resource_group_name", 'STR'),
            ("address_space", 'ARR')
        ]
    ])
    
def create_subnet(local_name: str) -> Response:
    """
    Creates a subnet.
    """
    return resource_template(local_name, ["azurerm_subnet",
        [
            ("name", 'STR'),
            ("virtual_network_name", 'STR'),
            ("resource_group_name", 'STR'),
            ("address_prefixes", 'ARR')
        ]
    ])