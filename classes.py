

class Response:
    def __init__(self, succ, data = [], type = '') -> None:

        if succ == False:
            self.success = succ
            self.data = ["Failed to create resource"]
            self.type = 'ERR'
        else:
            self.success = succ
            self.data = data
            self.type = type

    def get_type_string(self) -> str:
        if self.type == "ADD":
            return '+'
        elif self.type == "DEL":
            return '-'
        elif self.type == 'ERR':
            return '!'

    def print_response(self):
        type_s = self.get_type_string()
        # print(self.data)
        for line in self.data[:-1]:
            print("{} {}".format(type_s, line), end = '')

