from classes import Response

ADD = 'ADD'
DEL = 'DEL'
UPDATE = 'UPDATE'

reference_store = {
    'rgp': 'azurerm_resource_group',
    'vn': 'azurerm_virtual_network'
}


def check_for_variable(string) -> str:
    b_string = string.split(" ")
    if b_string[0] == "--":
         ref = b_string[1].split('.')
         popped = ref.pop(0)
         data = reference_store[popped]

         return data + '.' + '.'.join(ref)
    else:
        return  "\"" + string + "\"" 




def array_generator(address_string: str) -> str:
    add_space = address_string.split(" ")
    add_string = "["
    for i in range(0, len(add_space) - 1):
        add_string = add_string + "\"" + add_space[i] + "\"" + ", "
    add_string = add_string +  "\"" + add_space[-1] + "\"" + "]"
    return add_string


def resource_template(local_name: str, params: list = []):
    """
    [ resource_name, [(param_name, param_type), ... ] ]
    """
    try:
        header = "resource \"{}\" \"{}\" {{".format(params[0], local_name)
        print(header)
        output_array = []
        line_string = ""

        output_array.append(header + "\n")

        for parameter in params[1]:
            if parameter[1] != 'ARR':
                line_string = check_for_variable(input("\t{} = ".format(parameter[0])))
            else:
                line_string = input("\t{} = ".format(parameter[0])) 
                line_string = array_generator(line_string)

            output_array.append("\t{} = {}\n".format(parameter[0], line_string))
        
        output_array.extend(["}", "\n\n"])

        with open("demo.tf", mode = 'a') as tf_file:
            tf_file.writelines(output_array)

        return Response(True, output_array, ADD)
    
    except Exception as err:
        print("Error: {}".format(err))
        return Response(False)