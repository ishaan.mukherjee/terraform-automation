from util import check_resource_type
from resource_generators import *


if __name__ == "__main__":
    inp = ""
    while inp != "exit":
        inp = str(input("\n\n$ ")).lower()

        if inp != "exit":
            check_resource_type(inp)
