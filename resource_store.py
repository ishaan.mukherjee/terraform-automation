from resource_generators import *


resource_store = {
    "resource-group": create_resource_group,
    "rgp": create_resource_group,
    "resourcegroup": create_resource_group,

    "vn": create_virtual_network,
    "virtual-network": create_virtual_network,
    "virtualnetwork": create_virtual_network,

    "sbn": create_subnet,
    "sn": create_subnet,
    "subnet": create_subnet
}

resource_store_map = {
    'create_resource_group': 'azurerm_resource_group',
    'create_virtual_network': 'azurerm_virtual_network',
    'create_subnet': 'azurerm_subnet'
}